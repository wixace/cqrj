﻿using System.Collections;
using System.Collections.Generic;
using PGHY.PG;
using UnityEngine;
using UnityEngine.Events;

public class StoryManager : MonoBehaviour {

	public GameObject Finish;

	public UnityEvent Event;

	public static  StoryManager Instance;

	private int _piece;

	public int Piece
	{
		get { return _piece; }
		set
		{
			_piece=value;
			if(_piece==4) {
				Invoke("ShowFinish",3f);
			}
		}
	}

	void Awake() {
		Instance=this;
	}

	void Start(){GameController.Init();}
	void ShowFinish()
	{
		if(Event!=null)
		Event.Invoke();
		Finish.SetActive(true);
	}

}
