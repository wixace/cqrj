﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

public class UISwipe : MonoBehaviour,IDragHandler
{
	public BoxSlider BoxSlider;

	public Text Text;

	public float Speed = 0.05f;

	void Start() {
		GetComponent<RectTransform>().localPosition = Vector2.zero;
	}
	public void OnDrag(PointerEventData eventData) {
		float fMouseX = Input.GetAxis("Mouse X");
		float fMouseY = Input.GetAxis("Mouse Y");
		BoxSlider.ValueX+=fMouseX*Speed;
		BoxSlider.ValueY+=fMouseY*0.3f;
		if(fMouseX!=0)
		Text.text = fMouseX.ToString();
	}

	

}
