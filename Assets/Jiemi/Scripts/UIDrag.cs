﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIDrag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public bool dragOnSurfaces = true;

	private RectTransform m_DraggingPlane;

	public void OnBeginDrag(PointerEventData eventData) {
		var canvas = FindInParents<Canvas>(gameObject);
		if(canvas==null)
			return;


		if(dragOnSurfaces)
			m_DraggingPlane=transform as RectTransform;
		else
			m_DraggingPlane=canvas.transform as RectTransform;

		SetDraggedPosition(eventData);
	}

	public void OnDrag(PointerEventData data) {
		if(m_DraggingPlane!=null)
			SetDraggedPosition(data);
	}

	private void SetDraggedPosition(PointerEventData data) {
		if(dragOnSurfaces&&data.pointerEnter!=null&&data.pointerEnter.transform as RectTransform!=null)
			m_DraggingPlane=data.pointerEnter.transform as RectTransform;

		var rt = m_DraggingPlane.GetComponent<RectTransform>();
		Vector3 globalMousePos;
		if(RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane,data.position,data.pressEventCamera,out globalMousePos)) {
			rt.position=globalMousePos;
			rt.rotation=m_DraggingPlane.rotation;
		}
	}

	public void OnEndDrag(PointerEventData eventData) {
	
	}

	static public T FindInParents<T>(GameObject go) where T : Component {
		if(go==null)
			return null;
		var comp = go.GetComponent<T>();

		if(comp!=null)
			return comp;

		Transform t = go.transform.parent;
		while(t!=null&&comp==null) {
			comp=t.gameObject.GetComponent<T>();
			t=t.parent;
		}
		return comp;
	}
}
