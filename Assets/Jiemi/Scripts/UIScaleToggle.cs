﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
public class UIScaleToggle : MonoBehaviour {

	[SerializeField] private Vector3 _minSize = Vector3.one, _maxSize = Vector3.one*2;
	public float Height = 200, Width = 1285;
	private float _defaultWidth;
	public RectTransform RectTransform;

	// Use this for initialization
	void Start() {
		_defaultWidth=RectTransform.sizeDelta.x;
		GetComponent<Toggle>().onValueChanged.AddListener(v => {
			RectTransform.localScale=v ? _maxSize : _minSize;
			RectTransform.sizeDelta=v ? new Vector2(Width,Height) : new Vector2(_defaultWidth,0);
		});
	}

}
