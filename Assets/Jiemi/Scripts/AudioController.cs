﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioController : USingleton<AudioController> {

	private AudioSource _audioSource;

	private float _musicVolume = 1f;
	private float _soundVolume = 1f;
	public float MusicVolume
	{
		get { return _musicVolume; }
		set
		{
			_musicVolume=Mathf.Clamp01(value);
			_audioSource.volume=_musicVolume;
		}

	}

	public float SoundVolume
	{
		get { return _soundVolume; }
		set
		{
			_soundVolume=Mathf.Clamp01(value);
		}
	}

	void Awake() {
		_audioSource=GetComponent<AudioSource>();
	}

	public void Play(AudioClip audioClip) {
		_audioSource.clip=audioClip;
		_audioSource.Play();
	}

	public void PlayOneShot(AudioClip audioClip) {
		_audioSource.PlayOneShot(audioClip,_soundVolume);
	}
}
