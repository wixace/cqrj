﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Toggle))]
[RequireComponent(typeof(AudioSource))]
public class ToggleSfx : MonoBehaviour {

	// Use this for initialization
	void Start() {
		GetComponent<Toggle>().onValueChanged.AddListener(v => {
			if(v) {
				GetComponent<AudioSource>().volume=AudioController.Instance.SoundVolume;
				GetComponent<AudioSource>().enabled=true;

			} else
				GetComponent<AudioSource>().enabled=false;
		});
	}

}
