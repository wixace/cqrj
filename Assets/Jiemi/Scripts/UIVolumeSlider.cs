﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class UIVolumeSlider : MonoBehaviour {

	private Slider _slider;

	[SerializeField] private bool _musicControl=true, _soundControl=false;

	void Awake() {
		_slider=GetComponent<Slider>();
	}

	// Use this for initialization
	void Start() {

		AudioController.Instance.MusicVolume=_slider.value;
		AudioController.Instance.SoundVolume=_slider.value;

		_slider.onValueChanged.AddListener(v => {
			if(_musicControl)
				AudioController.Instance.MusicVolume=v;
			if(_soundControl)
				AudioController.Instance.SoundVolume=v;
		});
	}
}
