﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Piece : MonoBehaviour {


	public GameObject Result;
	public Sprite ZoomSprite, ZoomSpriteShine;
	public bool IsZoom;

	// Use this for initialization
	void Start() {
		GetComponent<Button>().onClick.AddListener(Zoom);
	}

	private void Zoom() {
		if(!IsZoom) {
			GetComponent<Animator>().enabled=true;
			GetComponent<Image>().sprite = ZoomSprite;
			IsZoom=true;
			Invoke("Shine",1f);
		} else {
			Result.SetActive(true);
			StoryManager.Instance.Piece++;
		}
	}

	private void Shine()
	{
		GetComponent<Image>().sprite=ZoomSpriteShine;
	}
}
