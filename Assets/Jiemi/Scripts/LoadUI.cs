﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadUI : MonoBehaviour {
	public GameObject UI;
	public UnityEvent OnLoad;
	public void Open() {
		UI.SetActive(true);
		if(OnLoad!=null)
			OnLoad.Invoke();
	}
}
