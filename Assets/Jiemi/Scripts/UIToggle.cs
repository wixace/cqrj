﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[RequireComponent(typeof(Toggle))]
public class UIToggle : MonoBehaviour {

	[SerializeField]
	protected UnityEvent _isOn, isOff;
	protected Toggle _toggle;
	protected RectTransform _rectTransform;
	public AudioClip _audio;
	private AudioSource _audioSource;

	/// <summary>
	/// 为Toggle监听事件
	/// </summary>
	protected void Awake() {
		_toggle=GetComponent<Toggle>();
		_rectTransform=GetComponent<RectTransform>();
		_audioSource=GetComponent<AudioSource>();
		AddListener((bool stat) => {
			if(stat)
				_isOn.Invoke();
			else
				isOff.Invoke();
			_audioSource.PlayOneShot(_audio);

		});
	}

	/// <summary>
	/// 添加监听器
	/// </summary>
	/// <param name="act"></param>
	protected void AddListener(Action<bool> act) {
		if(act!=null)
			_toggle.onValueChanged.AddListener((bool stat) => {
				act(stat);
			});
	}
}