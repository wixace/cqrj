﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIRestartButton : MonoBehaviour {

	// Use this for initialization
	void Start() {
		GetComponent<Button>().onClick.AddListener(() => {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		});
	}

}
