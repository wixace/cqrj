using System;
using UnityEngine;

public class CamFollower : MonoBehaviour {

	public Transform[] Points = new Transform[2];

	public SightGizmos SightGizmos;

	public float XMargin = 0f; //最小x移动间距
	public float YMargin = 0f; //最小y移动间距
	public float XSmooth = 8f; //x跟随速度
	public float YSmooth = 8f; //y跟随速度

	public Vector2 MaxXAndY = new Vector2(5,5); //最大x，y坐标
	public Vector2 MinXAndY = new Vector2(-5,-5); //最小x，y坐标
	public Vector2 ExactMaxXAndY = new Vector2(5,5); //实际最大x，y坐标
	public Vector2 ExactMinXAndY = new Vector2(-5,-5); //实际最小x，y坐标

	public Color GizmosColor = Color.black;

	private Transform _player;
	private Camera _camera;
	private float cameraHeight;
	private float cameraWidth;

	private void Awake() {
		_player=GameObject.FindGameObjectWithTag("Player").transform;
		_camera=GetComponent<Camera>();
	}

	/// <summary>
	/// 更新相机移动范围
	/// </summary>
	private void UpdateSightRange() {
		var pointA = Points[0];
		var pointB = Points[1];

		if(pointA!=null&&pointB!=null) {
			var isXBigger = pointA.transform.position.x>=pointB.transform.position.x;
			var isYBigger = pointA.transform.position.y>=pointB.transform.position.y;
			MaxXAndY.x=isXBigger ? pointA.transform.position.x : pointB.transform.position.x;
			MinXAndY.x=!isXBigger ? pointA.transform.position.x : pointB.transform.position.x;
			MaxXAndY.y=isYBigger ? pointA.transform.position.y : pointB.transform.position.y;
			MinXAndY.y=!isYBigger ? pointA.transform.position.y : pointB.transform.position.y;

			Vector2 middleXY;

			var middleX = MinXAndY.x>=0 ? (MinXAndY.x+MaxXAndY.x)/2 : MinXAndY.x+MaxXAndY.x;
			if(!float.IsInfinity(middleX/2))
				middleXY.x=middleX/2;
			else
				middleXY.x=middleX;

			var middleY = MinXAndY.y>=0 ? (MinXAndY.y+MaxXAndY.y)/2 : MinXAndY.y+MaxXAndY.y;
			if(!float.IsInfinity(middleY/2))
				middleXY.y=middleY/2;
			else
				middleXY.y=middleY;

			ExactMinXAndY=MinXAndY-new Vector2(middleXY.x-cameraWidth/2f-middleXY.x,middleXY.y-cameraHeight/2f-middleXY.y);
			ExactMaxXAndY=MaxXAndY-new Vector2(middleXY.x+cameraWidth/2f-middleXY.x,middleXY.y+cameraHeight/2f-middleXY.y);
		}

	}

	private bool CheckXMargin() {
		return Mathf.Abs(transform.position.x-_player.position.x)>XMargin;
	}

	private bool CheckYMargin() {
		return Mathf.Abs(transform.position.y-_player.position.y)>YMargin;
	}

	private void Update() {
		TrackPlayer();
	}

	/// <summary>
	/// 跟随玩家
	/// </summary>

	private void TrackPlayer() {

		float targetX = transform.position.x;
		float targetY = transform.position.y;


		if(CheckXMargin()) {
			targetX=Mathf.Lerp(transform.position.x,_player.position.x,XSmooth*Time.deltaTime);
		}

		if(CheckYMargin()) {
			targetY=Mathf.Lerp(transform.position.y,_player.position.y,YSmooth*Time.deltaTime);
		}

		targetX=Mathf.Clamp(targetX,ExactMinXAndY.x,ExactMaxXAndY.x);
		targetY=Mathf.Clamp(targetY,ExactMinXAndY.y,ExactMaxXAndY.y);
		transform.position=new Vector3(targetX,targetY,transform.position.z);
	}

	void OnDrawGizmos() {
		var cam = GetComponent<Camera>();
		cameraHeight=cam.orthographicSize*2;
		cameraWidth=cameraHeight*cam.aspect;
		UpdateSightRange();
		if(SightGizmos!=null) {
			SightGizmos.MaxXAndY=MaxXAndY;
			SightGizmos.MinXAndY=MinXAndY;
		}
	}

}
