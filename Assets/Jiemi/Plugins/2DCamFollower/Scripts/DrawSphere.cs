﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSphere : MonoBehaviour {

	public Color GizmosColor = Color.red;

	public float Radius = .2f;

	/// <summary>
	/// 绘制红点
	/// </summary>
	void OnDrawGizmos() {

		Gizmos.color=GizmosColor;
		Gizmos.DrawWireSphere(transform.position,Radius);

	}
}
