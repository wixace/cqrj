﻿using UnityEngine;

public class SightGizmos : MonoBehaviour {

	public Color GizmosColor = Color.cyan;

	public Vector2 MaxXAndY { get; set; }
	public Vector2 MinXAndY { get; set; }

	/// <summary>
	/// 绘制相机移动范围
	/// </summary>
	void OnDrawGizmos() {

		Gizmos.color=GizmosColor;
		Gizmos.matrix=transform.localToWorldMatrix;

		var topLeft = new Vector3(MinXAndY.x,MaxXAndY.y,transform.position.z);
		var topRight = new Vector3(MaxXAndY.x,MaxXAndY.y,transform.position.z);
		var btmRight = new Vector3(MaxXAndY.x,MinXAndY.y,transform.position.z);
		var btmLeft = new Vector3(MinXAndY.x,MinXAndY.y,transform.position.z);

		Gizmos.DrawLine(topLeft,topRight);
		Gizmos.DrawLine(topRight,btmRight);
		Gizmos.DrawLine(btmRight,btmLeft);
		Gizmos.DrawLine(btmLeft,topLeft);
	}
}
