﻿using WTF;
using UnityEngine;
using UnityEngine.UI;

public class GameManager:USingleton<GameManager>
{
    
    public Text Money, Point;

    public AudioClip AttackSfx;

    void Start() {
     
        InvokeRepeating("AddPoint", 1, 2);
        InvokeRepeating("PlaySfx", 1, 1);
        InvokeRepeating("AddMoney", 1, 1);
    }
    
    void PlaySfx() {
        FsAudioManager.Instance.PlayOneShot(AttackSfx);
    }

    public void AddMoney() {
        GameData.Money += 2;
        Money.text = GameData.Money.ToString();
    }

    public void AddPoint() {
        GameData.Point += 2;
        UpdatePoint();
    }

    public void UpdatePoint() {
        Point.text = GameData.Point.ToString();
    }
    public void SetSpeed(int spd) {
        Time.timeScale = spd;
    }
    
}
