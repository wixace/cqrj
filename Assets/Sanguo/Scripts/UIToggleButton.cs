﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Fs.Libs.UI
{
    public class UIToggleButton : MonoBehaviour
    {

        [SerializeField] private bool isOn,allowSwitchOff;

        [SerializeField] private UnityEvent IsOn,IsOff;

        [SerializeField] private Sprite OnSprite,OffSprite;
	
        private Image image;

        void Awake() => image = GetComponent<Image>();

        // Use this for initialization
        void Start () {
            GetComponent<Button>().onClick.AddListener(() =>
            {
                if(isOn&&!allowSwitchOff) return;
                
                isOn = !isOn;
                if (isOn)
                {
                    IsOn?.Invoke();
                    if(OnSprite!=null)
                        image.sprite=OnSprite;
                }
                else
                {
                    IsOff.Invoke();
                    if(OffSprite!=null)
                        image.sprite=OffSprite;
                }
            });
        }
	
    }
}
