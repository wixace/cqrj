﻿using UnityEngine;
using UnityEngine.UI;

public class SimpleCounter :MonoBehaviour
{
    public Text Counter;

    public Image Bar;
    public int Point;

    public void AddPoint() {
        if (GameData.Point > 0) {
            Bar.fillAmount = Point/30f;
            GameData.Point--;
            GameManager.Instance.UpdatePoint();
            Point++;
            Counter.text = Point.ToString();
        }
    }

}
