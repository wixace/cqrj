﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class UISlider : MonoBehaviour {

	[SerializeField]
	private float _span = 0.1f;

	protected Slider _slider;

	
	protected void Awake() => _slider=GetComponent<Slider>();

	protected void AddListener(Action<float> act) => _slider.onValueChanged.AddListener(v => {act?.Invoke(v);});

	public virtual void Increase()=>_slider.value+=_span;

	public virtual void Decrease()=>_slider.value-=_span;



}
