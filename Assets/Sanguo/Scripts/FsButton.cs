﻿using System;
using WTF;
using UnityEngine;

namespace Fs.View.Button {

    public abstract class FsButton : MonoBehaviour {

        [SerializeField] private bool _playSfx = true;
        private UnityEngine.UI.Button _button;

        protected abstract void OnClick();
        protected virtual void Awake() => _button = GetComponent<UnityEngine.UI.Button>();


        protected virtual void Start() {
            AddListener(() => {
                OnClick();
                if (_playSfx)
                    FsAudioManager.Instance.PlayButtonSfx();
            });

        }

        /// <summary>
        /// 添加监听器
        /// </summary>
        /// <param name="act"></param>
        protected void AddListener(Action act) {
            if (act != null)
                _button.onClick.AddListener(() => act());
        }

    }
}
