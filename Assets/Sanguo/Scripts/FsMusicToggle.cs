﻿using WTF;
using UnityEngine;

namespace Fs.View.Toggle {

    public class FsMusicToggle : FsToggle {


        void OnEnable() {
            IsOn = !FsAudioManager.Instance.IsMusicMute;
        }


        protected override void OnClick() {
            base.OnClick();
            FsAudioManager.Instance.ToggleMusic();
        }
    }
}
