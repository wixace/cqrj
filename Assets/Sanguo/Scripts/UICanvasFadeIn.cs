﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(CanvasGroup))]
public class UICanvasFadeIn : MonoBehaviour {

    [SerializeField] private float _duration = .5f;

    [SerializeField] private float _target = .5f;

    private CanvasGroup _canvasGroup;

    void Awake() => _canvasGroup = GetComponent<CanvasGroup>();

    void OnEnable() {
        Invoke("Close", 1f);
    }

    void Close() {
        _canvasGroup.alpha = 1f;
        _canvasGroup.DOFade(_target, _duration).OnComplete(()=> {
            gameObject.SetActive(false);
        });

    }
}
