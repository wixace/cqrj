﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Image))]
public class UIFadeOut : MonoBehaviour {

    [SerializeField] private float _duration=.5f;

    [SerializeField] private float _target=.5f;

    private Image _image;

    void Awake() => _image = GetComponent<Image>();

    void OnEnable() {
        Invoke("Close", 1f);
    }

    void Close() {
        _image.color = new Color(_image.color.r, _image.color.g, _image.color.b, 1f);
        _image.DOFade(_target, _duration).OnComplete(()=> {
            gameObject.SetActive(false);
        });

    }

}
