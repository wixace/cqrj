﻿using UnityEngine;

namespace WTF {
    [RequireComponent(typeof(AudioSource))]
    public sealed class FsAudioManager : USingleton<FsAudioManager> {


        [SerializeField] private AudioClip _buttonSfx;

        [SerializeField] private bool _saveSettings = true;

        private AudioSource _audioSource;

        private float _musicVolume = 1f;
        private float _soundVolume = 1f;
        public bool IsMusicMute =>!(MusicVolume>0);
        public bool IsSoundMute => !(SoundVolume > 0);

        public void UnMute() {
            _audioSource.mute = false;
            _audioSource.Play();
        }

        public void Mute() {
            _audioSource.mute = true;
        }


        public void ToggleMusic()
        {
            //_audioSource.mute = !_audioSource.mute;
            MusicVolume = MusicVolume < 1f ? 1f : 0f;
            //if (IsMusicMute)
            //    MusicVolume = 0;
            //else
            //    MusicVolume = 1;
        } 

        public void ToggleSound()
        {
            SoundVolume = SoundVolume < 1f ? 1f : 0f;
        } 

        public float MusicVolume {
            get => _musicVolume;
            set {
                _musicVolume = Mathf.Clamp01(value);
                _audioSource.volume = _musicVolume;
                //_audioSource.volume = _musicVolume;
                if (_saveSettings) {
                    PlayerPrefs.SetFloat("Music", _musicVolume);
                    PlayerPrefs.Save();
                }
            }
        }

        public float SoundVolume {
            get => _soundVolume;
            set {
                _soundVolume = Mathf.Clamp01(value);
                if (_saveSettings) {
                    PlayerPrefs.SetFloat("Sound", _soundVolume);
                    PlayerPrefs.Save();
                }
            }
        }

        protected  void Awake() {

            _audioSource = GetComponent<AudioSource>();
        }

        private void Start() {
            _musicVolume = PlayerPrefs.GetFloat("Music", 1f);
            _audioSource.volume = _musicVolume;
            //if (_musicVolume <= 0)
            //    ToggleMusic();
            _soundVolume = PlayerPrefs.GetFloat("Sound", 1f);
            //if (_musicVolume <= 0)
            //    ToggleSound();
        }

        public void PlayOneShot(AudioClip audioClip) {
            PlaySfx(audioClip);
        }

        public void PlayButtonSfx() {
            if (_buttonSfx != null) {
                PlaySfx(_buttonSfx);
            }
        }

        private void PlaySfx(AudioClip audioClip) {
            if (!IsSoundMute)
            {
                GameObject go = new GameObject("Audio: " + _buttonSfx.name);
                go.transform.parent = transform;
                //Create the source    
                AudioSource source = go.AddComponent<AudioSource>();
                source.clip = audioClip;
                source.volume = 1f;
                source.pitch = 1;
                source.Play();
                Destroy(go, audioClip.length);
                //return source;
            }
        }

        
        }
    }
