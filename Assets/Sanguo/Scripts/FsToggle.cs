﻿using Fs.View.Button;
using UnityEngine;
using UnityEngine.Events;

namespace Fs.View.Toggle {
    public class FsToggle : FsButton {

        [SerializeField] protected bool IsOn = true;

        [SerializeField] protected bool UseText = true;

        [SerializeField] protected UnityEvent OnToggleOn, OnToggleOff;

        [SerializeField] private Sprite _spriteOn, _spriteOff;

        [SerializeField] protected string TextOn = "On", TextOff = "Off";

        private UnityEngine.UI.Image _image;

        [SerializeField] protected UnityEngine.UI.Text _text;

        protected override void OnClick() {
            IsOn = !IsOn;
            if (IsOn) {
                OnToggleOn?.Invoke();
                ShowOnSprite();
                if (UseText)
                    _text.text = TextOn;
            } else {
                OnToggleOff?.Invoke();
                ShowOffSprite();
                if (UseText)
                    _text.text = TextOff;
            }
        }

        protected override void Awake() {
            base.Awake();
            _image = GetComponent<UnityEngine.UI.Image>();
        }

        protected void ShowOnSprite() {
            if (_spriteOn != null)
                _image.sprite = _spriteOn;
        }

        protected void ShowOffSprite() {
            if (_spriteOff != null)
                _image.sprite = _spriteOff;
        }
    }
}
