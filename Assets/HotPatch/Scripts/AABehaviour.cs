﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Networking;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AABehaviour : MonoBehaviour {
	[SerializeField] private GameObject    ChangelogView,         ErrorView,         LoadingView;
	[SerializeField] private Text          UpdateTitle,           UpdateText,        ErrorText, DownloadText;
	[SerializeField] private Button        PrivacyButton,         UpdateCloseButton, RetryButton;
	[SerializeField] private string        builtInScene = "Main", hotfixScene = "Game";
	[SerializeField] private UIProgressBar progressBar;

	public string bundle="Sanguo";
	
	private string[] Url = {
		"https://api2.bmob.cn/1/classes/List/b2ap666K",
		"https://sdk.panguhy.com/game/config?channel=700"
	};

	private Data                                _data;
	private string                              _idfa,           _idfv;
	private int                                 _currentVersion, _currentUrlIndex;
	private AsyncOperationHandle<SceneInstance> _loadOperation;
	private float                               downloadSize;
	private AsyncOperationHandle _loadependencies;

	protected virtual void OnEnable() => Connect();
	
	void Start() {
		//Caching.ClearCache();
		RetryButton.onClick.AddListener(() => {
			ErrorView.SetActive(false);
			ErrorView.SetActive(false);
			Connect();
		});
	}

#region Private Methods

	private void ShowChangelog() {
		//WebCover.SetActive(false);
		ChangelogView.SetActive(true);
	}

	private void HideChangelog() => ChangelogView.SetActive(false);

	private void PlayOfflineGame() {
		HideChangelog();
		SceneManager.LoadScene(builtInScene);
		//WebCover.SetActive(false);
		//	AudioManager.Instance.Play();
	}

	private void PlayOnlineGame() {
		progressBar.gameObject.SetActive(true);
		LoadImage.Url = _data.picUrl;
		LoadingView.SetActive(true);
		HideChangelog();

		//StartCoroutine(CheckDownloadSize());
		GetDownloadText();
	}

	public async void GetDownloadText() {
	

		downloadSize      = await Addressables.GetDownloadSizeAsync(bundle).Task / 1024f / 1024f;
		if (downloadSize > 0) {
			DownloadText.text = $"准备下载资源共{downloadSize:f2}M";
			await Task.Delay(2000);
			_loadependencies = Addressables.DownloadDependenciesAsync(bundle);
			_loadependencies.Completed += wtf => {
			  Addressables.LoadSceneAsync(hotfixScene);
			};	
			//_loadOperation = Addressables.LoadSceneAsync(hotfixScene);
		}
		else {
			DownloadText.text = $"检查完毕";
			progressBar.SetProgress(1f,true);
			await Task.Delay(2000);
			_loadOperation = Addressables.LoadSceneAsync(hotfixScene);
		}

	}

	void Update() {
		if (_loadOperation.IsValid()) {
			print("进度" + _loadOperation.PercentComplete);
		
		}
		if (_loadependencies.IsValid()) {
			DownloadText.text = $"正在下载{(_loadependencies.PercentComplete * downloadSize):f2}M/{downloadSize:f2}M";
			progressBar.SetProgress(_loadependencies.PercentComplete);
		}
	}
	

#endregion

	public void Connect() {
		if (Application.internetReachability != NetworkReachability.NotReachable) {
			ErrorView.SetActive(false);
			_currentVersion = PlayerPrefs.GetInt("Version", 0);
			StartCoroutine(GetData(data => {
				_data = data;
				PlayerPrefs.SetInt("Version", _data.version);
				PlayerPrefs.Save();
				if (_data.urlEnable) {
					PlayOnlineGame();
					//	InitUserProperty();
					return;
				}

				if (_data.updateEnable) {
					if (_currentVersion != _data.version || _data.forceUpdate) {
						UpdateText.text  = _data.updateText;
						UpdateTitle.text = _data.updateTitle;
						PrivacyButton.onClick.AddListener(() => { Application.OpenURL(_data.privacyUrl); });
						UpdateCloseButton.onClick.AddListener(() => {
							HideChangelog();
							PlayOfflineGame();
						});
						ShowChangelog();
					}
					else {
						HideChangelog();
						PlayOfflineGame();
					}
				}
				else {
					PlayOfflineGame();
				}
			}));
		}
		else {
			ErrorView.SetActive(true);
		}
	}

	IEnumerator GetData(Action<Data> onSuccess) {
		switch (_currentUrlIndex) {
			case 0:
				var headers = new Dictionary<string, string>();
				headers.Add("X-Bmob-Application-Id", "79b551daffd7fd1a4da10acaf8831d90");
				headers.Add("X-Bmob-REST-API-Key", "45f5e0cbe1f82cb837e6eb79ad641dc3");
				var www = new WWW(Url[_currentUrlIndex], null, headers);
				yield return www;
				if (!string.IsNullOrEmpty(www.text)) {
					var dat = JsonUtility.FromJson<Data>(www.text);
					onSuccess(dat);
					print(www.text);
				}
				else {
					HideChangelog();
					if (_currentUrlIndex < Url.Length - 1)
						_currentUrlIndex++;
					Connect();
				}

				break;
			default:
				UnityWebRequest uwr = UnityWebRequest.Get(Url[_currentUrlIndex]);
				yield return uwr.Send();
				if (uwr.isNetworkError || uwr.isHttpError) {
					Debug.Log(uwr.error);
					_currentUrlIndex++;
					if (_currentUrlIndex >= Url.Length) {
						_currentUrlIndex = 0;
						ErrorView.SetActive(true);
						ErrorText.text = "服务器连接失败,请稍后重试";
					}
					else {
						Connect();
					}
				}
				else {
					Debug.Log(System.Text.RegularExpressions.Regex.Unescape(uwr.downloadHandler.text));
					var dat = JsonUtility.FromJson<Data>(uwr.downloadHandler.text);
					onSuccess?.Invoke(dat);
				}

				break;
		}
	}

	[System.Serializable]
	private class Data {
		public int  version;      //版本号不一致时显示更新弹窗
		public bool urlEnable;    //链接开关
		public bool updateEnable; //开启更新弹窗

		public bool forceUpdate; //无视版本号显示更新弹窗

		//public string url;          //链接
		public string privacyUrl;  //隐私政策地址
		public string updateTitle; //更新弹窗标题
		public string updateText;  //更新弹窗内容
		public string picUrl;
	}
}