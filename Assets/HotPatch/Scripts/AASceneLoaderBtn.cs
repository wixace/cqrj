﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AASceneLoaderBtn : MonoBehaviour {
	public string address;
	private Text _text;
	private Button _button;

	private void Awake() {
		_button = GetComponent<Button>();
		_text = GetComponentInChildren<Text>();
	}

	void Start() {
		_button.onClick.AddListener(LoadScene);
	}

	public void LoadScene() {
		_text.text = "正在加载";
		Addressables.LoadSceneAsync(address).Completed += OnSceneLoaded;
		GetComponent<Button>().interactable = false;
	}

	private void OnSceneLoaded(AsyncOperationHandle<SceneInstance> obj) { }
}