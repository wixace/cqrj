﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AngleController : MonoBehaviour {

	[SerializeField] private RectTransform _rob; //棒棒

	[SerializeField] private RectTransform _frame; //框框

	[SerializeField] private UnityEvent _onStart;

	[SerializeField] private Text _angleText;

	[SerializeField] private float _sensitivity = 10f;

	private bool _isStart, _isEnabled;

	public void EnableControl() {
		_isEnabled=true;
	}

	void Update() {

		if(Input.anyKeyDown&&!_isStart)
			if(_onStart!=null) {
				_onStart.Invoke();
				_isStart=true;
			}

		if(!_isEnabled)
			return;

		var axis = Input.GetAxis("Horizontal");

		float rotation = -axis*_sensitivity;
		_rob.Rotate(0,0,rotation);

		if(Input.GetKeyDown(KeyCode.Space)||Input.GetKeyDown(KeyCode.Joystick1Button0)) {

			_angleText.gameObject.SetActive(true);
			var angle = _rob.transform.eulerAngles.z>=180 ? 360-_rob.transform.eulerAngles.z: _rob.transform.eulerAngles.z;
			if (angle >= 90f) angle -= 180f;
			_angleText.text="垂直角度为:"+Mathf.Abs(angle)+"°";
		}

	}

}
