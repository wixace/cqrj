﻿using UnityEngine;
using UnityEngine.Events;

public class ObjectDetector : MonoBehaviour {

	public UnityEvent OnEnter, OnExit;

	[SerializeField] private string _tag = "Player";

	void OnTriggerEnter(Collider coll) {
		if(coll.CompareTag(_tag)) {
			if(OnEnter!=null)
				OnEnter.Invoke();
		}
	}

	void OnTriggerExit(Collider coll) {
		if(coll.CompareTag(_tag)) {
			if(OnExit!=null)
				OnExit.Invoke();
		}
	}


	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.CompareTag(_tag)) {
			if (OnEnter != null)
				OnEnter.Invoke();
		}
	}

	void OnTriggerExit2D(Collider2D coll) {
		if (coll.CompareTag(_tag)) {
			if (OnExit != null)
				OnExit.Invoke();
		}
	}

}
