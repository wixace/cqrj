﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.Events;

public class RandomRotation : MonoBehaviour {

	[Range(-3,3)]
	[SerializeField]
	private float _minSpeed = -1;
	[Range(-3,3)]
	[SerializeField]
	private float _maxSpeed = 1;

	private float _speed = 0.5f;

	[Range(0,10)]
	[SerializeField]
	private float _minDuration = 3;
	[Range(0,10)]
	[SerializeField]
	private float _maxDuration = 6;

	private float _duration;

	private float _direction;

	private Stopwatch _stopwatch = new Stopwatch();

	public UnityEvent OnDone;

	void OnEnable() {
		var rand = Random.Range(0,2);
		_speed=Random.Range(_minSpeed,_maxSpeed);
		_direction=Mathf.CeilToInt(rand)==0 ? -1 : 1;
		_duration=Random.Range(_minDuration,_maxDuration);
		_stopwatch.Reset();
		_stopwatch.Start();
	}

	void Update() {
		if(_stopwatch.Elapsed.Seconds<_duration) {
			transform.Rotate(Vector3.back*_direction*_speed);
		} else {
			_stopwatch.Stop();
			if(OnDone!=null)
				OnDone.Invoke();
			enabled=false;
		}
	}

}
