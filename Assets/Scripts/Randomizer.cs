﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Randomizer : MonoBehaviour {
	[SerializeField] private Dice[] dices;

	[SerializeField] private GameObject randBtn;

	[SerializeField] private AnimationCurve animCurve;

	[SerializeField] private float speed = 0.5f;

	public GameObject startView,resultView;
	private AudioSource audioSource;

	private int index = 0;

	public Image _target;
	public int   _targetInt;

	public Text result;

	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	private void OnEnable() {
		_targetInt     = Random.Range(0, dices.Length - 1);
		_target.sprite = dices[_targetInt].GetComponentsInChildren<Image>()[2].sprite;
		startView.SetActive(true);
	}

	public void StartRandom() {
		randBtn.SetActive(false);
		dices[index].LightOff();
		foreach (var dice in dices)
			dice.InputOff();
		StartCoroutine(RandomCoroutine());
	}

	IEnumerator RandomCoroutine() {
		float duration  = Random.Range(5f, 7f),
		      startTime = Time.time,
		      time      = 0f;

		int size = dices.Length;

		index = Random.Range(0, size);

		dices[index].LightOn();

		while (time < duration) {
			time = Time.time - startTime;
			dices[index].LightOff();
			index = (index < (size - 1)) ? ++index : 0;
			dices[index].LightOn();
			audioSource.Play();
			yield return new WaitForSeconds(animCurve.Evaluate(time / 2f) * speed);
		}

		randBtn.SetActive(true);
		foreach (var dice in dices)
			dice.InputOn();

		if (index == _targetInt)
			result.text = "恭喜选中了！";
		else
			result.text = "很遗憾没选中！";

		resultView.SetActive(true);
	}
}
