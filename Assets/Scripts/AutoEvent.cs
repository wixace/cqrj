﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AutoEvent : MonoBehaviour {

	[SerializeField] private UnityEvent _unityEvent;

	[SerializeField] private float _delay;


	void OnEnable() => Invoke("Excute", _delay); 

	void Excute()
	{
		_unityEvent?.Invoke();
	}
}
