﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(CircleCollider2D))]

public class BasketballController : MonoBehaviour
{

	public List<Color> Colors;

	public UnityEvent OnReset,OnFinish;

    Vector2 offset;
    Vector2 lastPosition;
    Vector2 currentPosition;
    Vector2 startPosition = new Vector2(0, -3);

    SpriteRenderer rimOverlay,renderer;

    Rigidbody2D basketball;

    BoxCollider2D rim;
    BoxCollider2D rimLeft;
    BoxCollider2D rimRight;

	private Tweener tweener;

    public float maxVelocity = 10;
    float gravity = 2;

    float screenBoundBottom = -8;
    float screenBoundTop = 12;
    float screenBoundLeft = -10;
    float screenBoundRight = 10;

    float shotBound = .75F;
    float rimBound = 1.9F;

    bool thrown = false;

	private int score;

	public GameObject Hoop;

	public Transform Pos1, Pos2;

	private bool isLeft = true;

	private Rigidbody2D rigidbody;

	void Start()
    {
        basketball = gameObject.GetComponent<Rigidbody2D>();
        basketball.gravityScale = 0;
	    renderer = GetComponent<SpriteRenderer>();
		rim = GameObject.Find("Rim").GetComponent<BoxCollider2D>();
        rimRight = GameObject.Find("RimRight").GetComponent<BoxCollider2D>();
        rimLeft = GameObject.Find("RimLeft").GetComponent<BoxCollider2D>();
        rimOverlay = GameObject.Find("RimOverlay").GetComponent<SpriteRenderer>();
	    rigidbody = GetComponent<Rigidbody2D>();

		gameObject.transform.position = startPosition;
        currentPosition = transform.position;
    }

	public void AddScore()
	{
		score++;
		if (score >= 5)
		{
			OnFinish?.Invoke();
			score = 0;
			Reset();
			//score = 0;
			return;
			//enabled = false;
		}
		isLeft = !isLeft;
		Hoop.transform.position = isLeft ? Pos1.position : Pos2.position;
		//print(score);
	}

    void Update()
    {
	    if (Input.GetMouseButton(0)) {
		    basketball.velocity = Vector2.zero;
		    rigidbody.AddForce(new Vector2((0 - Camera.main.ScreenToWorldPoint(Input.mousePosition).x) * 5, 15), ForceMode2D.Impulse);
		    basketball.gravityScale = gravity; // disable gravity while mouse down
		    thrown = true;
	    }

		lastPosition = currentPosition;
        currentPosition = transform.position;

        if(thrown)
        {
            if (!rim.enabled && lastPosition.y - currentPosition.y > 0) // ball has negavtive trajectory
            {
                ToggleRim();
            }
            else if(rim.enabled && lastPosition.y - currentPosition.y < 0) // ball has positive trajectory
            {
                ToggleRim();
            }

	        if (transform.position.y < screenBoundBottom)
	        {
		        thrown = false;
				Invoke("Reset", .5f);
	        }
	        //Reset();
	        else if (transform.position.x < screenBoundLeft || transform.position.x > screenBoundRight)
	        {
		        thrown = false;
				Invoke("Reset", .5f);
	        }
	        //Reset();

		}

        else if (transform.position.y > shotBound) // release ball when dragged past shooting range
            OnMouseUp();
        
        /* make ball smaller as it goes away
        else
        {
            if((currentPosition - lastPosition).y > 0F) // ball moving up
            {
                gameObject.transform.localScale.Scale(new Vector3(.001, .001));
            }
        }
        */
    }

    // ToggleRimColliders() - activates/deactivates rim edge colliders and overlay sprite
    // called after ball begins rising/falling to allow it to fall into hoop rather than hitting the bottom
    // TODO: tune rim overlay logic
    void ToggleRim()
    {
		rimLeft.enabled = !rimLeft.enabled;
		rimRight.enabled = !rimRight.enabled;
		rim.enabled = !rim.enabled;

		//if (rimRight.isActiveAndEnabled)
		//	Debug.Log("rim ACTIVATED");
		//else
		//	Debug.Log("rim deactivated");

		rimOverlay.enabled = !rimOverlay.enabled;
    }

    // Reset() - resets game after ball goes out of bounds
    void Reset()
    {
	    renderer.color = Colors[score];
	    tweener?.Kill();
	    OnReset?.Invoke();
		gameObject.transform.position = startPosition;
        basketball.gravityScale = 0;
        basketball.velocity = Vector2.zero;
        thrown = false;

        // freeze rotation after respawning ball
        basketball.freezeRotation = true;
        basketball.freezeRotation = false;

        ToggleRim();
	    transform.localScale = Vector3.one;

	}

	// OnMouseDown() - called when player first grabs ball
	// configures ball to be dragged around by player

	void FixedUpdate()
	{
		
	}
	void OnMouseDown()
    {
		
		//if(!thrown)
		//{
			//print(Camera.main.ScreenToWorldPoint(Input.mousePosition) + "" + transform.position);
			//rigidbody.AddForce(new Vector2((transform.position.x- Camera.main.ScreenToWorldPoint(Input.mousePosition).x)*10, 12),ForceMode2D.Impulse);	
			//    //_screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

			//    offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(
			//        new Vector2(Input.mousePosition.x, Input.mousePosition.y));

			   //basketball.gravityScale = 0; // disable gravity while mouse down
			//    basketball.velocity = new Vector2(0,0);
		//}

	}

    // OnMouseUp() - called when player releases ball to be thrown
    // gives ball tragectory and sets thrown = true
    void OnMouseUp()
    {
     //   // get direction of movement in last frame
     //   // move object in that direction at velocity
        if(!thrown)
       {
     //       Vector2 velocity = (currentPosition - lastPosition) * 20;
     //       if (velocity.y > maxVelocity)
     //       {
     //           //velocity.y = maxVelocity
     //           float yDiff = velocity.y - maxVelocity;
     //           velocity.y -= yDiff;
     //           //velocity.x -= yDiff;
     //       }

     //       basketball.velocity = velocity;
           basketball.gravityScale = gravity; // disable gravity while mouse down

           thrown = true;
        }
	    //tweener= transform.DOScale(.8f, 1f);
    }

    void OnMouseDrag()
    {
        //if(!thrown)
        //{
        //    Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        //    Vector2 curPosition = (Vector2) Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

        //    gameObject.transform.position = curPosition;
        //}
    }


}