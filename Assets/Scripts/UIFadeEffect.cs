﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIFadeEffect : MonoBehaviour {

	[SerializeField] private float _duration;

	[SerializeField] private float _inValue = 1f, _outValue = 0f, _startValue=0f;

	[SerializeField] private FadeType _fadeType;

	private MaskableGraphic _image;



	void Awake() => _image=GetComponent<MaskableGraphic>();
	// Use this for initialization
	void OnEnable()
	{
		_image.color = new Color(_image.color.r, _image.color.g, _image.color.b,_startValue);
		_image.DOFade(_fadeType == FadeType.FadeIn ? _inValue : _outValue, _duration);
	}

	private enum FadeType { FadeIn, FadeOut }

}
