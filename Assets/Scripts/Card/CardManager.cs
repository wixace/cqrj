﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : USingleton<CardManager> {


	public GridLayoutGroup Board; //格子布局

	public GameObject GamePanel, GameOverDialog; //游戏面板

	public Card[] Prefabs; //预制物体卡牌

	public Text Hint, Time, Record; //提示和时间文本

	private List<int> _cardHolder, _cardList; //所有id列表和已随机id列表

	private Card[] _cards; //所有卡牌脚本

	private int _currentHolding = 0, _currentLeft = 0; //当前翻开卡牌数量(不包括已成功配对的)

	private Card _lastCard; //上一个被翻开的卡牌

	private Stopwatch _stopwatch = new Stopwatch();

	private int _row, _col, _level;

	/// <summary>
	/// 翻开指定卡牌
	/// </summary>
	/// <param name="card"></param>
	public void HoldCard(Card card) {
		if(++_currentHolding>=2) {
			foreach(var t in _cards) {
				if(t!=null)
				t.Disable();
			}
			if(card.Id==_lastCard.Id) //如果匹配
			{
				//_cardList.Remove(card.Id);
				--_currentLeft;
				_lastCard.IsRevealed=true;
				card.IsRevealed=true;

				Hint.text="还剩"+_currentLeft+"对碎片";
				//print("还剩"+_cardList.Count+"对");
				if(_currentLeft<=0) {
					Hint.text="通关了,好棒！";
					GameOverDialog.SetActive(true);
					var record = PlayerPrefs.GetInt(_level.ToString(),9999);
					int time = (int)(_stopwatch.ElapsedMilliseconds/1000);
					if(time<record) {
						Record.text=time.ToString();
						PlayerPrefs.SetInt(_level.ToString(),time);
						PlayerPrefs.Save();
					} else {
						Record.text=record.ToString();
					}

					_stopwatch.Stop();
					return;
				}

				//print("游戏结束");
			}
			Invoke("EnableToggles",1f);
			_currentHolding=0;
		} else {
			_lastCard=card;
		}
	}

	void EnableToggles() {
		foreach(var t in _cards) {
			if(!t.IsRevealed) {
				t.Enable();
				t.Close();
			}
		}
	}

	void Update() {
		Time.text=(_stopwatch.ElapsedMilliseconds/1000).ToString();
	}

	void Reset() {
		foreach(RectTransform c in Board.transform) {
			GameObject.Destroy(c.gameObject);
		}
		_stopwatch.Reset();
		_currentHolding=0;
		_lastCard=null;
	}

	/// <summary>
	/// 开始游戏
	/// </summary>
	/// <param name="row"></param>
	/// <param name="col"></param>
	public void StartGame(int row,int col,int level) {
		_row=row;
		_col=col;
		_level=level;
		Reset();
		Board.constraintCount=col;
		Init();
		Generate(row*col/2);
		RandomDeck();
		_stopwatch.Start();
		_currentLeft=_cardList.Count;
		//Hint.text="还剩"+_cardList.Count+"对";
		Hint.text="还剩"+_currentLeft+"对碎片";

		GamePanel.SetActive(true);
	}

	public void Restart() {
		StartGame(_row,_col,_level);
	}

	/// <summary>
	/// 初始化
	/// </summary>
	private void Init() {
		_cardHolder=new List<int>();
		_cardList=new List<int>();
		for(int i = 0; i<Prefabs.Length; i++) {
			_cardHolder.Add(i);
		}
	}

	/// <summary>
	/// 生成卡牌
	/// </summary>
	/// <param name="count"></param>
	public void Generate(int count) {
		for(int i = 0; i<count; i++) {
			var r = _cardHolder[Random.Range(0,_cardHolder.Count)];
			var go = Instantiate(Prefabs[r]);
			go.transform.SetParent(Board.transform);
			var go2 = Instantiate(Prefabs[r]);
			go2.transform.SetParent(Board.transform);
			_cardHolder.Remove(r);
			_cardList.Add(r);
		}
		_cards=Board.transform.GetComponentsInChildren<Card>();
		print(_cards.Length);
	}

	public void RandomDeck() {
		var childCount = Board.transform.childCount;
		foreach(Transform t in Board.transform) {
			t.SetSiblingIndex(Random.Range(0,childCount));
		}
	}
}

#region Draft
//using System.Collections;
//using System.Collections.Generic;
//using System.Diagnostics;
//using UnityEngine;
//using UnityEngine.UI;

//public class CardManager : USingleton<CardManager> {


//	public GridLayoutGroup Board; //格子布局

//	public GameObject GamePanel, GameOverDialog; //游戏面板

//	public Card[] Prefabs; //预制物体卡牌

//	public Text Hint, Time; //提示和时间文本

//	private List<int> _cardHolder, _cardList; //所有id列表和已随机id列表

//	private Card[] _cards; //所有卡牌脚本

//	private int _currentHolding = 0, _currentLeft = 0; //当前翻开卡牌数量(不包括已成功配对的)

//	private Card _lastCard; //上一个被翻开的卡牌

//	private Stopwatch _stopwatch = new Stopwatch();

//	private int _row, _col;

//	/// <summary>
//	/// 翻开指定卡牌
//	/// </summary>
//	/// <param name="card"></param>
//	public void HoldCard(Card card) {
//		if(++_currentHolding>=2) {
//			print(_currentHolding);
//			foreach(var t in _cards) {
//				t.Disable();
//			}
//			if(card.Id==_lastCard.Id) //如果匹配
//			{
//				_cardList.Remove(card.Id);
//				_lastCard.IsRevealed=true;
//				card.IsRevealed=true;

//				Hint.text="还剩"+_cardList.Count+"对";
//				//print("还剩"+_cardList.Count+"对");
//				if(_cardList.Count==0) {
//					Hint.text="通关了,好棒！";
//					GameOverDialog.SetActive(true);
//					_stopwatch.Stop();
//					return;
//				}

//				//print("游戏结束");
//			}
//			Invoke("EnableToggles",1f);
//			_currentHolding=0;
//		} else {
//			_lastCard=card;
//		}
//	}

//	void EnableToggles() {
//		foreach(var t in _cards) {
//			if(!t.IsRevealed) {
//				t.Enable();
//				t.Close();
//			}
//		}
//	}

//	void Update() {
//		Time.text=(_stopwatch.ElapsedMilliseconds/1000).ToString();
//	}

//	void Reset() {
//		foreach(RectTransform c in Board.transform) {
//			GameObject.Destroy(c.gameObject);
//		}
//		_stopwatch.Reset();
//		_currentHolding=0;
//		_lastCard=null;
//	}

//	/// <summary>
//	/// 开始游戏
//	/// </summary>
//	/// <param name="row"></param>
//	/// <param name="col"></param>
//	public void StartGame(int row,int col) {
//		_row=row;
//		_col=col;
//		Reset();
//		Board.constraintCount=col;
//		Init();
//		Generate(row*col/2);
//		RandomDeck();
//		_stopwatch.Start();
//		_currentLeft=_cardList.Count;
//		//Hint.text="还剩"+_cardList.Count+"对";
//		Hint.text="还剩"+_currentLeft+"对";

//		GamePanel.SetActive(true);
//	}

//	public void Restart() {
//		StartGame(_row,_col);
//	}

//	/// <summary>
//	/// 初始化
//	/// </summary>
//	private void Init() {
//		_cardHolder=new List<int>();
//		_cardList=new List<int>();
//		for(int i = 0; i<Prefabs.Length; i++) {
//			_cardHolder.Add(i);
//		}
//	}

//	/// <summary>
//	/// 生成卡牌
//	/// </summary>
//	/// <param name="count"></param>
//	public void Generate(int count) {
//		for(int i = 0; i<count; i++) {
//			var r = _cardHolder[Random.Range(0,_cardHolder.Count)];
//			var go = Instantiate(Prefabs[r]);
//			go.transform.SetParent(Board.transform);
//			var go2 = Instantiate(Prefabs[r]);
//			go2.transform.SetParent(Board.transform);
//			_cardHolder.Remove(r);
//			_cardList.Add(r);
//		}
//		_cards=new Card[0];
//		_cards=Board.transform.GetComponentsInChildren<Card>();
//	}

//	public void RandomDeck() {
//		var childCount = Board.transform.childCount;
//		foreach(Transform t in Board.transform) {
//			t.SetSiblingIndex(Random.Range(0,childCount));
//		}
//	}
//}

#endregion