﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardGameStarter : MonoBehaviour {

	[Range(2,4)] public int Row = 2;

	[Range(2,8)] public int Column = 2; //游戏的行列数

	public int Level = 1;

	private Button _button;

	private void Awake() {
		_button=GetComponent<Button>();
	}

	private void Start() {
		_button.onClick.AddListener(StartGame);
	}

	public void StartGame() {
		//b = GetComponent();
		CardManager.Instance.StartGame(Row,Column,Level); //开始游戏
	}

}
