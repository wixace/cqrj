﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {

	public int Id;

	public string SpriteName;

	public string AnimalName;

	public Image _image;

	public Text _text;

	private Toggle _toggle;

	public bool IsRevealed { get; set; }

	void Awake() {
		_toggle=GetComponent<Toggle>();
	}

	// Use this for initialization
	void Start() {
		_toggle.onValueChanged.AddListener((bool s) => {
			if(s) {
				CardManager.Instance.HoldCard(this);
				_toggle.interactable=false;
			}
		});
		_text.text=AnimalName;
		_image.sprite=Resources.Load<Sprite>(SpriteName);
		_image.preserveAspect = true;
	}

	public void Close() {
		_toggle.isOn=false;
	}

	public void Disable() {
		_toggle.interactable=false;
	}

	public void Enable() {
		_toggle.interactable=true;
	}
}
