﻿using UnityEngine;
using UnityEngine.UI;

public class Dice : MonoBehaviour {

    [SerializeField]
    private Image highlight;

    [SerializeField]
    private InputField inputField;

    public int id=0;

    public void Start()
    {
        inputField.text=PlayerPrefs.GetString(id.ToString(), "Mooji");
        inputField.onEndEdit.AddListener((string str)=>
        {
            PlayerPrefs.SetString(id.ToString(), str);
            PlayerPrefs.Save();
        });
    }

    #region Commands
    public void LightOn()
    {
        highlight.enabled = true;
    }

    public void InputOff()
    {
        inputField.interactable = false;
    }

    public void InputOn()
    {
        inputField.interactable = true;
    }

    public void LightOff()
    {
        highlight.enabled = false;
    }
    #endregion

}
